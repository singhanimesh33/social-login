package com.example.demo.POC.MedVan.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.POC.MedVan.Service.IUserService;
import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.exception.ResourceNotFoundException;
import com.example.demo.POC.MedVan.repository.UserRepository;
import com.example.demo.POC.MedVan.security.UserPrincipal;

@Service
public class UserService implements IUserService{
	
	@Autowired
    private UserRepository userRepository;
	
	@Override
	public User getCurrentUser(UserPrincipal userPrincipal) {
	return	userRepository.findById(userPrincipal.getId())
        .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
	}

	@Override
	public List<User> restricted(){
		return userRepository.findAll();
	}
}
