package com.example.demo.POC.MedVan.Service;

import javax.validation.Valid;

import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.payload.LoginRequest;
import com.example.demo.POC.MedVan.payload.SignUpRequest;

public interface IAuthService {

	String authenticateUser(@Valid LoginRequest loginRequest);

	User registerUser(SignUpRequest signUpRequest);

}
