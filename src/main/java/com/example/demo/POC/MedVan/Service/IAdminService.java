package com.example.demo.POC.MedVan.Service;

import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.payload.SignUpRequest;

public interface IAdminService {

	User doRegistration(SignUpRequest signUpRequest);

	String restricted();

	String restrictedEmployee();

}
