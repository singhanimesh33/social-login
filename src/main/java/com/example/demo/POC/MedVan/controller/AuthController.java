package com.example.demo.POC.MedVan.controller;

import static com.example.demo.POC.MedVan.constants.Constants.USER_REGISTRATION_SUCCESSFUL;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.POC.MedVan.Service.IAuthService;
import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.exception.UserEmailPasswordIncorrectException;
import com.example.demo.POC.MedVan.payload.ApiResponse;
import com.example.demo.POC.MedVan.payload.AuthResponse;
import com.example.demo.POC.MedVan.payload.LoginRequest;
import com.example.demo.POC.MedVan.payload.SignUpRequest;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private IAuthService authService;

	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		try {
			String token = authService.authenticateUser(loginRequest);
			return ResponseEntity.ok(new AuthResponse(token));
		} catch (UserEmailPasswordIncorrectException ex) {
			throw new UserEmailPasswordIncorrectException(ex.getMessage());

		}
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

		User result = authService.registerUser(signUpRequest);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/user/me")
				.buildAndExpand(result.getId()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, USER_REGISTRATION_SUCCESSFUL));
	}

}
