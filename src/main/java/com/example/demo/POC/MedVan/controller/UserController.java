package com.example.demo.POC.MedVan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.POC.MedVan.Service.IUserService;
import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.security.CurrentUser;
import com.example.demo.POC.MedVan.security.UserPrincipal;

@RestController
public class UserController {

	@Autowired
	private IUserService userService;

	@GetMapping("/user/me")
	@PreAuthorize("hasRole('USER')")
	public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
		return userService.getCurrentUser(userPrincipal);
	}

	@GetMapping("/restricted")
	@PreAuthorize("hasRole('USER')")
	public List<User> restricted() {
		return userService.restricted();
	}
}
