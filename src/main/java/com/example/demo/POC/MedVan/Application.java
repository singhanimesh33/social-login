package com.example.demo.POC.MedVan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.example.demo.POC.MedVan.config.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class Application {


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
