package com.example.demo.POC.MedVan.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.POC.MedVan.Service.IAdminService;
import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.payload.SignUpRequest;

@RestController
public class AdminController {

	@Autowired
	private IAdminService adminService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

	@PostMapping("/register")
	@PreAuthorize("hasRole('ADMIN')")
	public User doRegistration(@RequestBody @Valid SignUpRequest signUpRequest, HttpServletRequest httpServletRequest) {
		LOGGER.info("inside do Registartion method of Authentication Controller for user " + "registration ");
		return adminService.doRegistration(signUpRequest);

	}

	@GetMapping("/adminaccess")
	@PreAuthorize("hasRole('ADMIN')")
	public String restricted() {
		return adminService.restricted();
	}

	@GetMapping("/employeeaccess")
	@PreAuthorize("hasRole('EMPLOYEE')")
	public String restrictedemployee() {
		return adminService.restrictedEmployee();
	}
}
