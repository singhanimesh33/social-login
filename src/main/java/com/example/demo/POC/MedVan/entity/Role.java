package com.example.demo.POC.MedVan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Role.
 */
@Entity
@Table(name = "roles")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role  {

 // private static final long serialVersionUID = 7758761816659731714L;

  /**
   * The Role id.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private long roleId;

  /**
   * The Role.
   */
  //@Column(name = "role", updatable = false, nullable = false, unique = true)
  private String role;

  /**
   * The User.
   */
 /* @JsonIgnore
  @ManyToMany(fetch = FetchType.EAGER,
      mappedBy = "roles")
 // @ManyToMany(targetEntity = User.class, mappedBy = "roles", cascade = CascadeType.ALL)
  private Set<User> users = new HashSet<>();*/
}