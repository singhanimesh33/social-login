package com.example.demo.POC.MedVan.payload;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExceptionResponse {
  private Date timestamp;
  private String message;
  private String details;
  private String httpCodeMessage;
}
