package com.example.demo.POC.MedVan.security;


import com.example.demo.POC.MedVan.exception.ResourceNotFoundException;
import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.exception.UserEmailPasswordIncorrectException;
import com.example.demo.POC.MedVan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rajeevkumarsingh on 02/08/17.
 */

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email)
            throws UserEmailPasswordIncorrectException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() ->
                      //  new UsernameNotFoundException("User not found with email : " + email)
         new UserEmailPasswordIncorrectException("Incorrect Credentials")
        );

        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
            () -> new ResourceNotFoundException("User", "id", id)
        );

        return UserPrincipal.create(user);
    }
}