package com.example.demo.POC.MedVan.Service.impl;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.POC.MedVan.Service.IAdminService;
import com.example.demo.POC.MedVan.entity.AuthProvider;
import com.example.demo.POC.MedVan.entity.Role;
import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.exception.BadRequestException;
import com.example.demo.POC.MedVan.payload.SignUpRequest;
import com.example.demo.POC.MedVan.repository.UserRepository;
import static com.example.demo.POC.MedVan.constants.Constants.*;

@Service
public class AdminService implements IAdminService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public User doRegistration(SignUpRequest signUpRequest) {
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			throw new BadRequestException("Email address already in use.");
		}
		User user = new User();
		Role role = new Role();
		role.setRoleId(2);
		role.setRole(EMPLOYEE);
		HashSet<Role> hashSet = new HashSet<>();
		hashSet.add(role);
		user.setName(signUpRequest.getName());
		user.setEmail(signUpRequest.getEmail());
		user.setPassword(signUpRequest.getPassword());
		user.setProvider(AuthProvider.local);

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRoles(hashSet);
		return userRepository.save(user);

	}

	@Override
	public String restricted() {
		return "you have got admin privilege";
	}

	@Override
	public String restrictedEmployee() {
		return "you have got employee privilege";
	}
}
