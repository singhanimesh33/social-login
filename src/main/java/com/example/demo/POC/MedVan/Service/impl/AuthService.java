package com.example.demo.POC.MedVan.Service.impl;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.POC.MedVan.Service.IAuthService;
import com.example.demo.POC.MedVan.entity.AuthProvider;
import com.example.demo.POC.MedVan.entity.Role;
import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.exception.BadRequestException;
import com.example.demo.POC.MedVan.payload.LoginRequest;
import com.example.demo.POC.MedVan.payload.SignUpRequest;
import com.example.demo.POC.MedVan.repository.UserRepository;
import com.example.demo.POC.MedVan.security.TokenProvider;
import static com.example.demo.POC.MedVan.constants.Constants.*;

@Service
public class AuthService implements IAuthService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TokenProvider tokenProvider;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public String authenticateUser(LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		return tokenProvider.createToken(authentication);
	}

	@Override
	public User registerUser(SignUpRequest signUpRequest) {
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			throw new BadRequestException(EMAIL_IN_USE);
		}

		// Creating user's account
		User user = new User();
		Role role = new Role();
		role.setRoleId(1);
		role.setRole(USER);
		HashSet<Role> hashSet = new HashSet<>();
		hashSet.add(role);
		user.setName(signUpRequest.getName());
		user.setEmail(signUpRequest.getEmail());
		user.setPassword(signUpRequest.getPassword());
		user.setProvider(AuthProvider.local);

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRoles(hashSet);
		return userRepository.save(user);

	}
}
