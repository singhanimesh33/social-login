package com.example.demo.POC.MedVan.exception;

import java.util.Date;
//import com.tricon.schmersal.model.ExceptionDto;
import com.example.demo.POC.MedVan.payload.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(UserEmailPasswordIncorrectException.class)
  public final ResponseEntity<ExceptionResponse> handleUserEmailPasswordIncorrectException(UserEmailPasswordIncorrectException ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
        request.getDescription(false), HttpStatus.OK.getReasonPhrase());
    return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.OK);
  }
}
