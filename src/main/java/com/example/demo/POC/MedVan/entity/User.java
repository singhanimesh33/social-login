package com.example.demo.POC.MedVan.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The type Users.
 */
@Entity
@Table(name = "users")
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User  {

  /**
   * The User id.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private long id;

  /**
   * The User name.
   */
  @Column(name = "name", nullable = false)
  private String name;

  /**
   * The User email.
   */
  @Column(name = "email", nullable = false)
  private String email;


  @NotNull
  @Enumerated(EnumType.STRING)
  private AuthProvider provider;

  private String providerId;

  private String imageUrl;
/*
  @Column(nullable = false)
  private Boolean emailVerified = false;*/

  @JsonIgnore
  @Column(name = "password")
  private String password;


  /**
   * The Created by.
   */
  @Column(name = "created_by")
  private long createdBy;

  /**
   * The Is archived.
   */
  @Column(name = " is_deleted")
  private Boolean isArchived;
  /**
   * List of Authorities.
   */
  @ManyToMany(fetch = FetchType.EAGER,
      cascade = {
          CascadeType.MERGE
      })
  @JoinTable(name = "user_role_map",
      joinColumns = {@JoinColumn(name = "user_id")},
      inverseJoinColumns = {@JoinColumn(name = "role_id")})
  // @ManyToMany(targetEntity = Role.class,cascade = CascadeType.ALL )
  private Set<Role> roles = new HashSet<>();
}
 /* @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.roles.stream()
        .map(r -> new SimpleGrantedAuthority("ROLE_" + r.getRole()))
        .collect(Collectors.toSet());
  }

  @Override
  public String getUsername() {
    return String.valueOf(this.id);
  }

  @Override
  public boolean isAccountNonExpired() {
    return false;
  }

  @Override
  public boolean isAccountNonLocked() {
    return false;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return false;
  }

  @Override
  public boolean isEnabled() {
    return false;
  }
}*/
