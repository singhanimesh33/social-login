package com.example.demo.POC.MedVan.Service;

import java.util.List;

import com.example.demo.POC.MedVan.entity.User;
import com.example.demo.POC.MedVan.security.UserPrincipal;

public interface IUserService {

	User getCurrentUser(UserPrincipal userPrincipal);

	List<User> restricted();

}
