package com.example.demo.POC.MedVan.entity;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
