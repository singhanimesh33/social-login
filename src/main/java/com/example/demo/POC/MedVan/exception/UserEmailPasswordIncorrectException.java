package com.example.demo.POC.MedVan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type User email password incorrect exception.
 */
@ResponseStatus(HttpStatus.OK)
public class UserEmailPasswordIncorrectException extends RuntimeException {

    /**
     * Instantiates a new User email password incorrect exception.
     *
     * @param message the message
     */
    public UserEmailPasswordIncorrectException(final String message) {

        super(message);
    }

}
