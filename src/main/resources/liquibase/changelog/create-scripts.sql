--liquibase formatted sql

--changeset script:1 runOnChange:false runAlways:false
CREATE TABLE users(
    id BIGINT AUTO_INCREMENT,
    password varchar(100) ,
    email varchar(50) ,
    name varchar(50) NOT NULL,
    provider varchar(50) ,
    provider_id varchar(50) ,
    image_url varchar(250) ,
    created_by BIGINT ,
    is_deleted BOOLEAN,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);


--changeset script:2 runOnChange:false runAlways:false
CREATE TABLE roles (
	id BIGINT AUTO_INCREMENT,
	role varchar(100) NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT role_pk PRIMARY KEY (id),
	UNIQUE (role)
);

--changeset script:3 runOnChange:false runAlways:false
CREATE TABLE user_role_map (
	id BIGINT AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	CONSTRAINT FK_user_map FOREIGN KEY (user_id) REFERENCES users(id),
	CONSTRAINT FK_role_map FOREIGN KEY (role_id) REFERENCES roles(id)
);
